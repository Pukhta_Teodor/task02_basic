package com.pukhta;

import java.util.Scanner;

/**
 * Main program class.
 */

public class Main {
    /**
     * Lab program that builds an array
     * by entered interval and Fibonacci sequance,
     * First 2 numbers of Fibonacci sequance are last 2 numbers
     * entered by user in array with interval.
     *
     * @param args You can enter some String parameters from command line
     * @author Teodor Pukhta
     * @version 1.0
     */
    public static void main(final String[] args) {
        int fibSize;

        EvenOddArray evenOddsArray = new EvenOddArray();
        evenOddsArray.run();

        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\nEnter size of Fibonacci array:");
        fibSize = scanner.nextInt();
        Fibonacci fibonacci = new Fibonacci(fibSize, evenOddsArray.getMaxOdd(),
                                                evenOddsArray.getMaxEven());
        fibonacci.run();
    }
}
