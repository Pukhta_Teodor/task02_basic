package com.pukhta;

/**
 * Class describes Fibonacci sequance.
 */
public class Fibonacci {
    /**
     * arry of fibonacci numbers
     */
    private int[] fibArray;

    /**
     * size of fibonacci array
     */
    private int fibSize;

    /**
     * number of odd numbers in fibonacci sequence
     */
    private int fibSumOdd; // number of odds

    /**
     * number of even numbers in fibonacci sequence
     */
    private int fibSumEven; // number of evens

    /**
     * percent of even numbers in fibonacci sequence
     */
    private double fibPercentOdd;

    /**
     * percent of even numbers in fibonacci sequence
     */
    private double fibPercentEven;


    /**
     * Constructor of Fibonacci class
     *
     * @param size  - size of sequance
     * @param numb1 - first number of sequance
     * @param numb2 - second number of sequance
     */
    Fibonacci(final int size, final int numb1, final int numb2) {
        fibSize = size;

        fibArray = new int[fibSize];
        fibArray[0] = numb1;
        fibArray[1] = numb2;
        buildFibArray();

        fibSumEven = 0;
        fibSumOdd = 0;
        fibPercentEven = 0;
        fibPercentOdd = 0;

    }

    /**
     * building a fibonacci sequence
     */
    public final void buildFibArray() {
        for (int i = 2; i < fibSize; i++) {
            fibArray[i] = fibArray[i - 2] + fibArray[i - 1];
        }
    }

    /**
     * start work of class
     */
    public final void run() {
        calcOddEven();
        calcPercent();
        System.out.println("\nPercent of odd numbers:" + fibPercentOdd
                + "\nPercent of even numbers:" + fibPercentEven);
    }

    /**
     * calculating number of odd and even numbers in fibonacci sequence
     */
    public final void calcOddEven() {

        for (int i = 0; i < fibSize; i++) {
            if (fibArray[i] % 2 == 0) {
                fibSumOdd++;
            } else {
                fibSumEven++;
            }
        }
    }

    /**
     * calculating percent of odd and even numbers in fibonacci sequence
     */
    public final void calcPercent() {
        /** maximum percent*/
        int maxPercent = 100;
        fibPercentOdd = (double) fibSumOdd / fibSize * maxPercent;
        fibPercentEven = (double) fibSumEven / fibSize * maxPercent;
    }


}
